package com.yanyuan.mybaits;

import com.yanyuan.mybaits.config.SqlSession;
import com.yanyuan.mybaits.config.SqlSessionFactory;
import com.yanyuan.mybaits.dao.IMessage;
import com.yanyuan.mybaits.dao.IUser;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
