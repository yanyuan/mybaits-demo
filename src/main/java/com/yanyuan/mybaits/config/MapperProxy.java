package com.yanyuan.mybaits.config;


import com.yanyuan.mybaits.config.xml.Command;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * @Description mapper代理类
 * @Author yanyuan
 * @Date 11:54 2020/3/18
 * @Version 1.0
 **/
@Slf4j
public class MapperProxy<T> implements InvocationHandler {

    private final SqlSession sqlSession;
    private final Map<Method, Command> methodCache;


    public MapperProxy(SqlSession sqlSession, Map<Method, Command> methodCache) {
        this.sqlSession = sqlSession;
        this.methodCache = methodCache;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        MapperMethod mapperMethod = new MapperMethod(method, methodCache);
        mapperMethod.execute(sqlSession, args);
        return null;
    }
}
