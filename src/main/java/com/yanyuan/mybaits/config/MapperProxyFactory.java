package com.yanyuan.mybaits.config;

import com.yanyuan.mybaits.config.xml.Command;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;

/**
 * @Description mapper代理工厂类
 * @Author yanyuan
 * @Date 13:29 2020/3/18
 * @Version 1.0
 **/
public class MapperProxyFactory<T> {

    private final Class<T> mapperInterface;

    private final Map<Method, Command> methodCache;


    public MapperProxyFactory(Class<T> mapperInterface, Map<Method, Command> methodCache) {
        this.mapperInterface = mapperInterface;
        this.methodCache = methodCache;
    }

    protected T newInstance(MapperProxy<T> mapperProxy) {
        return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[]{mapperInterface}, mapperProxy);
    }

    public T newInstance(SqlSession sqlSession) {
        MapperProxy<T> mapperProxy = new MapperProxy(sqlSession, methodCache);
        return newInstance(mapperProxy);
    }
}
