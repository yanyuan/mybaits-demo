package com.yanyuan.mybaits.config;

/**
 * @Description 数据库操作类
 * @Author yanyuan
 * @Date 11:27 2020/3/18
 * @Version 1.0
 **/
public class SqlSession {

    MapperRegistry mapperRegistry;

    public SqlSession(MapperRegistry mapperRegistry) {
        this.mapperRegistry = mapperRegistry;
    }

    public <T> T getMapper(Class<T> type){
        return mapperRegistry.getMapper(type, this);
    }
}
