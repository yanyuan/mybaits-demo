package com.yanyuan.mybaits.config.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import lombok.Data;

/**
 * @Description SQL执行命令
 * @Author yanyuan
 * @Date 14:42 2020/3/18
 * @Version 1.0
 **/
@Data
@XStreamAlias("command")//对应mapper元素
public class Command {

    @XStreamAsAttribute
    private String id;

    @XStreamAsAttribute
    private String type;

    @XStreamAlias("content")
    private String content;
}
