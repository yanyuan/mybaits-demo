package com.yanyuan.mybaits.config.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description TODO
 * @Author yanyuan
 * @Date 14:53 2020/3/18
 * @Version 1.0
 **/
@Data
@XStreamAlias("mapper")//对应mapper元素
public class Mapper {

    @XStreamAsAttribute
    private String namespace;

    @XStreamImplicit(itemFieldName = "command")
    private List<Command> commands = new ArrayList<>();
}
