package com.yanyuan.mybaits.dao;

import com.yanyuan.mybaits.entity.Message;

import java.util.List;

/**
 * @Description 消息持久化
 * @Author yanyuan
 * @Date 11:24 2020/3/18
 * @Version 1.0
 **/
public interface IMessage {

    Message getById(Long messageId);
    List<Message> queryList(String title, String from);
    int insert(Message message);
}
