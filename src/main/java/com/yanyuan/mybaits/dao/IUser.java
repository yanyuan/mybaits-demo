package com.yanyuan.mybaits.dao;

import com.yanyuan.mybaits.entity.User;

import java.util.List;

/**
 * @Description 用户持久化
 * @Author yanyuan
 * @Date 11:24 2020/3/18
 * @Version 1.0
 **/
public interface IUser {

    User getById(Long userId);

    List<User> queryList(String name, String nickName);
}
