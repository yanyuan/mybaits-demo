package com.yanyuan.mybaits.entity;

import lombok.Data;

/**
 * @Description 消息
 * @Author yanyuan
 * @Date 11:24 2020/3/18
 * @Version 1.0
 **/
@Data
public class Message {

    private Long id;

    private String title;

    private String content;

    private String form;

    private String to;

    private String createdTime;
}
