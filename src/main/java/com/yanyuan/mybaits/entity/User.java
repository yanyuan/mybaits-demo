package com.yanyuan.mybaits.entity;

import lombok.Data;

/**
 * @Description 用户
 * @Author yanyuan
 * @Date 11:24 2020/3/18
 * @Version 1.0
 **/
@Data
public class User {

    private Long id;

    private String name;

    private String nickName;


    private String createdTime;
}
