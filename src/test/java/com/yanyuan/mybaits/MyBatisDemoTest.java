package com.yanyuan.mybaits;

import com.yanyuan.mybaits.config.SqlSession;
import com.yanyuan.mybaits.config.SqlSessionFactory;
import com.yanyuan.mybaits.dao.IMessage;
import com.yanyuan.mybaits.dao.IUser;

/**
 * @Description mybatis测试类
 * @Author yanyuan
 * @Date 17:40 2020/3/18
 * @Version 1.0
 **/
public class MyBatisDemoTest {

    public static void main(String[] args){
        SqlSession sqlSession = SqlSessionFactory.createSqlSession();

        IMessage iMessage = sqlSession.getMapper(IMessage.class);
        iMessage.getById(1L);
        iMessage.queryList("title", "zhangsan");

        //调用未实现方法
//        Message entity = new Message();
//        iMessage.insert(entity);


        IUser iUser = sqlSession.getMapper(IUser.class);
        iUser.getById(1001L);
        iUser.queryList("李逵", "黑旋风");
    }
}
